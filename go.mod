module stdreportx

go 1.22.1

require (
	github.com/go-sql-driver/mysql v1.8.0
	github.com/lib/pq v1.10.9
	gopkg.in/yaml.v3 v3.0.1
)

require filippo.io/edwards25519 v1.1.0 // indirect
