package main

import (
	"database/sql"
	"errors"
	"log"
	"os"
	"time"

	"gopkg.in/yaml.v3"
)

func openDB() (*sql.DB, error) {
	db, err := sql.Open("mysql", conf.Db)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func processConfig() {
	confdata := readFile("/etc/stdreport.yml")
	yaml.Unmarshal(confdata, &conf)

}

func readFile(f string) []byte {
	content, err := os.ReadFile(f)
	if err != nil {
		return []byte("")
	}
	return content
}

func getDBS() (*sql.DB, error) {
	dbN := randomArray(conf.DBS)
	dbConnect := false
	var db *sql.DB
	var err error
	qstring := ""
	for i := range dbN {
		switch conf.DBType {
		case "postgres":
			if conf.DBPassword == "" {
				qstring = "postgres://" + conf.DBUsername + "@" + dbN[i] + "/" + conf.DBName
			} else {
				qstring = "postgres://" + conf.DBUsername + ":" + conf.DBPassword + "@" + dbN[i] + "/" + conf.DBName
			}
			if conf.DBParam != "" {
				qstring = qstring + "?" + conf.DBParam
			}
		case "mysql":
			if conf.DBPassword == "" {
				qstring = conf.DBUsername + "@tcp(" + dbN[i] + ":3306)/" + conf.DBName
			} else {
				qstring = conf.DBUsername + ":" + conf.DBPassword + "@tcp(" + dbN[i] + ":3306)/" + conf.DBName
			}
			if conf.DBParam != "" {
				qstring = qstring + "?" + conf.DBParam
			}
		}

		log.Printf("Try db %s\n", dbN[i])
		db, err = sql.Open(conf.DBType, qstring)
		if err != nil {
			log.Printf("Error: fail to open db %s - %v\n", dbN[i], err)
			continue
		}

		err = db.Ping()
		if err != nil {
			log.Printf("Error: fail to ping db %s - %v\n", dbN[i], err)
			db.Close()
			continue
		}
		log.Printf("Connect to db %s\n", dbN[i])
		dbConnect = true
		break
	}
	if !dbConnect {
		log.Printf("Error: Cannot connect to db\n")
		return nil, errors.New("cannot connect to db")
	}
	return db, nil
}

func randomArray(data []string) []string {
	output := make([]string, 0)
	input := make([]string, len(data))
	copy(input,data)
	for {
		if len(input) == 0 {
			return output
		}
		appmutex.Lock()
		choose := randomx.Intn(len(input))
		appmutex.Unlock()
		output = append(output, input[choose])
		input = deleteArray(input, choose)
	}
}

func deleteArray(data []string, n int) []string {
	return append(data[:n], data[n+1:]...)
}

func getTimeStamp(t time.Time) string {
	return t.Format("2006-01-02T15:04:05")
}
